import setuptools

with open("odoo/addons/somconnexio/README.md") as f:
    long_description = f.read()

setuptools.setup(
    setup_requires=["setuptools-odoo"],
    long_description=long_description,
    long_description_content_type="text/markdown",
    odoo_addon={
        "depends_override": {
            "helpdesk_mgmt": "odoo12-addon-helpdesk-mgmt==12.0.1.26.0.99.dev35",
            "helpdesk_type": "odoo12-addon-helpdesk-type==12.0.1.1.0.99.dev4",
            "knowledge": "odoo12-addon-knowledge==12.0.1.0.1",
        }
    }
)